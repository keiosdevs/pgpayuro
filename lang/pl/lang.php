<?php

return [
    'strings' => [
        'goto' => 'Przejdź do',
        'andlogin' => 'i zaloguj się za pomocą swojego konta sprzedawcy PayU.',
        'click' => 'Kliknij',
        'myshops' => 'Moje Sklepy',
        'andselect' => 'i wybierz sklep, który chcesz zintegrować.',
        'ifyouhaveno' => 'Jeśli nie masz zdefiniowanego żadnego sklepu, kliknij <b>Dodaj sklep</b> i stwórz nowy sklep używając <b>Rest API</b> i adresu URL: ',
        'posbutton' => 'Przejdź do zakładki <b>POS</b> i wybierz POS, którego chcesz użyć.',
        'ifnopos' => 'Jeśli nie masz zdefiniowanych żadnych punktów POS, kliknij <b>Dodaj POS - Checkout</b> i stwórz nowy punkt POS.',
        'greenbox' => 'Kliknij nazwę punktu POS na liście. Zobaczysz zielone pole podobne do poniższego:',
        'firstpos' => 'Skopiuj pierwszą pozycję (pos_id) do pola Twoje ID POS poniżej.',
        'secondpos' => 'Skopiuj drugą pozycję (klucz MD5) do pola Twoja sygnatura (suma kontrolna MD5) poniżej.',
        'savesettings' => 'Wyłącz tryb testowy i zapisz ustawienia.',
    ],
    'labels' => [
        'pluginDesc' => 'Rozszerzenie wspierające PayU Polska dla Bramy Płatności Keios',
    ],
    'operators' => [
        'payuro' => 'PayU Rumunia',
    ],
    'settings' => [
        'tab' => 'PayU RO',
        'general' => 'Ustawienia ogólne',
        'posId' => 'Twoje ID POS',
        'signatureKey' => 'Twoja sygnatura (suma kontrolna MD5)',
        'testMode' => 'Tryb testowy',
        'merchant_id' => 'ID',
        'merchant_key' => 'Klucz',
    ],
    'info' => [
        'header' => 'Jak znaleźć klucze do API PayU',
    ],
];