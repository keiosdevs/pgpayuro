<?php

return [
    'strings'   => [
        'goto'         => 'Go to',
        'andlogin'     => 'and log into your PayU merchant account.',
        'click'        => 'Click',
        'myshops'      => 'My Shops',
        'andselect'    => 'and select shop that you want to integrate.',
        'ifyouhaveno'  => 'If you have no shops defined, click <b>Add Shop</b> button and create a new shop using <b>Rest API</b> with url: ',
        'posbutton'    => 'Click <b>POS</b> button / tab and select POS you want to use.',
        'ifnopos'      => 'If you have no POS points defined, click <b>Add POS - Checkout</b> button and create a new POS point.',
        'greenbox'     => 'Click on the POS name in POS points list. You will see a green box named <b>Configuration keys</b> looking like this:',
        'firstpos'     => 'Copy first position indicated with an arrow (pos_id) from <b>Configuration keys</b> to Your POS ID field below.',
        'secondpos'    => 'Copy second position indicated with an arrow (MD5 key) from <b>Configuration keys</b> to Your Signature Key (MD5 checksum) field below.',
        'savesettings' => 'Turn off test mode and save settings.',
    ],
    'labels'    => [
        'pluginDesc' => 'PayU RO Integration Extension for Keios PaymentGateway',
    ],
    'operators' => [
        'payuro' => 'PayU Romania',
    ],
    'settings'  => [
        'tab'          => 'PayU RO',
        'general'      => 'General settings',
        'posId'        => 'Your POS ID',
        'signatureKey' => 'Your Signature Key (MD5 checksum)',
        'testMode'     => 'Sandbox mode',
        'merchant_id'  => 'Merchant ID',
        'merchant_key' => 'Merchant Key',
    ],
    'info'      => [
        'header' => 'How to retrieve your PayU Credentials',
    ],
];