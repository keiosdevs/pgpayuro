<?php namespace Keios\PGPayURO\Classes;

use Carbon\Carbon;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use InvalidArgumentException;
use Keios\MoneyRight\Money;
use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\Core\Operator;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use RuntimeException;
use System\Behaviors\SettingsModel;

/**
 * Class PayURoConnector
 *
 * @package Keios\PGPayURO
 */
class PayURoConnector
{

    use SettingsDependent;

    /**
     * @var Operator
     */
    private $payment;

    /**
     *
     */
    const POST_URL = 'https://secure.payu.ro/order/lu.php';

    /**
     *
     */
    const CONFIRM_URL = 'https://secure.payu.ro/order/idn.php';

    /**
     *
     */
    const REFUND_URL = 'https://secure.payu.ro/order/irn.php';

    /**
     *
     */
    private $orderedSerializableFields = [
        'MERCHANT',
        'ORDER_REF',
        'ORDER_DATE',
        'ORDER_PNAME',
        'ORDER_PCODE',
        'ORDER_PINFO',
        'ORDER_PRICE',
        'ORDER_QTY',
        'ORDER_VAT',
        'PRICES_CURRENCY',
        'DISCOUNT',
        'DESTINATION_CITY',
        'DESTINATION_STATE',
        'DESTINATION_COUNTRY',
        'PAY_METHOD',
        'ORDER_PGROUP',
        'ORDER_PRICE_TYPE'
    ];

    /**
     * @var bool
     */
    private $shouldLog;

    /**
     * @var Log
     */
    private $logger;

    /**
     * PayURoConnector constructor.
     *
     * @param Operator $payment
     * @param boolean  $shouldLog
     */
    public function __construct(Operator $payment, Log $log, $shouldLog)
    {
        $this->payment = $payment;
        $this->shouldLog = $shouldLog;
        $this->logger = $log;
    }

    /**
     * @param Orderable $cart
     * @param Details   $details
     *
     * @return array
     */
    private function prepareFields(Orderable $cart, Details $details)
    {
        /**
         * @var SettingsModel $settings
         */
        $settings = $this->getSettings();
        $hashKey = $settings->get('payu.ro.merchant_key');
        $mode = (boolean)$settings->get('payu.ro.mode');
        $merchantKey = $settings->get('payu.ro.merchant_id');

        $url = URL::to(
            '_paymentgateway/'.OperatorUrlizer::urlize($this->payment).
            '?pgUuid='.base64_encode($this->payment->uuid)
        );

        $currency = $cart->getTotalNetCost()->getCurrency()->getIsoCode();

        $fields = [
            'MERCHANT'         => $merchantKey,
            'ORDER_REF'        => (string)$this->payment->uuid,
            'ORDER_DATE'       => (string)$this->payment->created_at,

            // order details
            'ORDER_PNAME'      => [],
            'ORDER_PCODE'      => [],
            'ORDER_PRICE'      => [],
            'ORDER_PRICE_TYPE' => [],
            'ORDER_QTY'        => [],
            'ORDER_VAT'        => [],
            'PRICES_CURRENCY'  => (string)$currency,
            'LANGUAGE'         => 'EN', // TODO: language per locale
            'BACK_REF'         => $url,
            'TESTORDER'        => 'FALSE',
        ];

        if ($mode === true) {
            $fields['TESTORDER'] = 'TRUE';
        }

        foreach ($cart->getAll() as $item) {
            array_push($fields['ORDER_PNAME'], $item->getDescription());
            array_push($fields['ORDER_PCODE'], $item->getId());

            array_push($fields['ORDER_PRICE'], $item->getSingleGrossPrice()->getAmountBasic());
            array_push($fields['ORDER_QTY'], $item->getCount());

            array_push($fields['ORDER_PRICE_TYPE'], 'GROSS');
            array_push($fields['ORDER_VAT'], '0');
        }

        $fields['ORDER_HASH'] = HmacHasher::calculate(
            $this->orderForHmacHashing(
                $this->filterForHmacHashing($fields)
            ),
            $hashKey
        );

        $flattenedFields = [];
        foreach ($fields as $fieldName => $fieldValue) {
            $fieldName = strtoupper($fieldName);

            if (is_array($fieldValue)) {
                for ($i = 0; $i < count($fieldValue); $i++) {
                    $fieldValue[$i] = htmlspecialchars($fieldValue[$i]);
                    $flattenedFields[$fieldName.'['.$i.']'] = $fieldValue[$i];
                }
            } else {
                $fieldValue = htmlspecialchars($fieldValue);
                $flattenedFields[$fieldName] = $fieldValue;
            }
        }

        return $flattenedFields;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function sendConfirmation(array $data)
    {
        /**
         * @var SettingsModel $settings
         */
        $settings = $this->getSettings();
        $hashKey = $settings->get('payu.ro.merchant_key');
        \Log::info(print_r($data,true));

        $parameters = [
            'MERCHANT'       => $data['MERCHANT'],
            'ORDER_REF'      => $data['ORDER_REF'],
            'ORDER_AMOUNT'   => $data['ORDER_AMOUNT'],
            'ORDER_CURRENCY' => $data['ORDER_CURRENCY'],
            'IDN_DATE'       => Carbon::now()->toDateTimeString(),
        ];

        $parameters['ORDER_HASH'] = HmacHasher::calculate($parameters, $hashKey);

        list($results, $output) = $this->executePostRequest(self::CONFIRM_URL, $parameters);

        try {
            list($receivedHash, $calculatedHash, $code, $status) = $this->parseResults($output, $results, $hashKey);

            if ($receivedHash !== $calculatedHash) {
                return false;
            }

            if ($code !== 1) {
                return false;
            }

            return true;
        } catch (InvalidArgumentException $ex) {
            $this->logError($ex->getMessage());

            return false;
        }
    }

    /**
     * @param Orderable $cart
     * @param Details   $details
     *
     * @return PaymentResponse
     */
    public function prepareRedirect(Orderable $cart, Details $details)
    {
        return new PaymentResponse($this->payment, self::POST_URL, null, $this->prepareFields($cart, $details), false);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function filterForHmacHashing(array $data)
    {
        $filtered = [];

        foreach ($data as $key => $value) {
            if (!in_array($key, $this->orderedSerializableFields)) {
                continue;
            }

            $filtered[$key] = $value;
        }

        return $filtered;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function orderForHmacHashing(array $data)
    {
        $ordered = [];
        foreach ($this->orderedSerializableFields as $key) {
            if (array_key_exists($key, $data)) {
                $ordered[$key] = $data[$key];
            }
        }

        return $ordered;
    }

    /**
     * @param       $url
     * @param array $parameters
     *
     * @return mixed
     */
    private function executePostRequest($url, array $parameters)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        preg_match_all("/<EPAYMENT>(.*)<\/EPAYMENT>/", $output, $results);

        curl_close($ch);

        return [$results, $output];
    }

    /**
     * @return PaymentResponse
     */
    public function sendRefundRequest()
    {
        /**
         * @var SettingsModel $settings
         */
        $settings = $this->getSettings();
        $hashKey = $settings->get('payu.ro.merchant_key');
        $merchantKey = $settings->get('payu.ro.merchant_id');

        $orderRef = $this->payment->payurefno;

        if (is_null($orderRef)) {
            throw new RuntimeException("Payment {$this->payment->uuid} doesn't have it's PayU RefNo.");
        }

        /**
         * @var Money $money
         */
        $money = $this->payment->amount;
        $amount = $money->getAmountBasic();
        $currency = $money->getCurrency()->getIsoCode();

        $parameters = [
            'MERCHANT'       => $merchantKey,
            'ORDER_REF'      => $orderRef,
            'ORDER_AMOUNT'   => $amount,
            'ORDER_CURRENCY' => $currency,
            'AMOUNT'         => $amount,
            'IRN_DATE'       => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        $parameters['ORDER_HASH'] = HmacHasher::calculate($parameters, $hashKey);

        list($results, $output) = $this->executePostRequest(self::REFUND_URL, $parameters);

        try {
            list($receivedHash, $calculatedHash, $code, $status) = $this->parseResults($output, $results, $hashKey);

            $errors = [];

            if ($receivedHash !== $calculatedHash) {
                $this->logError(
                    sprintf(
                        'Invalid response from PayU Api - security failure. Calculated hash: %s, received hash %s',
                        $calculatedHash,
                        $receivedHash
                    )
                );

                $errors[] = 'Invalid response from server - security failure.';
            }

            if ($code !== 1) {
                $this->logError(
                    sprintf(
                        ''
                    )
                );

                $errors[] = $status;
            }

            return new PaymentResponse($this->payment, null, $errors, null);
        } catch (InvalidArgumentException $ex) {
            $this->logError($ex->getMessage());

            return new PaymentResponse($this->payment, null, [$ex->getMessage()], null);
        }
    }

    /**
     * @param string $output
     * @param array  $results
     * @param        $hashKey
     * @param int    $expectedCount
     *
     * @return array
     * @throws InvalidArgumentException
     */
    private function parseResults($output, array $results, $hashKey, $expectedCount = 5)
    {
        if (count($results[1]) === 0) {
            throw new InvalidArgumentException(
                'Server response does not contain valid XML element, dumping response:'.PHP_EOL.$output
            );
        }

        $response = $results[1][0];

        $parts = explode('|', $response);

        if (count($parts) !== $expectedCount) {
            throw new InvalidArgumentException(
                'Parts in response do not match expected count, dumping response:'.PHP_EOL.$output
            );
        }

        $receivedHash = array_pop($parts);
        $calculatedHash = HmacHasher::calculate($parts, $hashKey);
        $code = (int)array_shift($parts);
        $status = array_shift($parts);

        return [$receivedHash, $calculatedHash, $code, $status];
    }

    /**
     * @param string $msg
     */
    private function logInfo($msg)
    {
        if ($this->shouldLog) {
            $this->logger->log('info', $msg);
        }
    }

    /**
     * @param string $msg
     */
    private function logError($msg)
    {
        if ($this->shouldLog) {
            $this->logger->log('error', $msg);
        }
    }
}