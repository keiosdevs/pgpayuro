<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 1/24/16
 * Time: 7:59 PM
 */

namespace Keios\PGPayURO\Classes;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

/**
 * @package Keios\PGPayURO\Classes
 */
class HmacHasher {

    /**
     * @param array $map
     * @param       $key
     *
     * @return string
     */
    public static function calculate(array $map, $key) {
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($map)); // flattens
        $it->rewind(); // back to start
        $list = iterator_to_array($it, false); // flattened result
        $result = "";

        foreach ($list as $value) {
            $result .= (string)mb_strlen($value).$value;
        }

        return hash_hmac("md5", $result, $key);
    }
}