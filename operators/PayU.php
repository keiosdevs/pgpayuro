<?php namespace Keios\PGPayURO\Operators;

use Carbon\Carbon;
use Finite\StatefulInterface;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Keios\PaymentGateway\Exceptions\CancellationFailureException;
use Keios\PaymentGateway\Exceptions\AcceptanceFailureException;
use Keios\PaymentGateway\Exceptions\RefundFailureException;
use Keios\PaymentGateway\Exceptions\RejectionFailureException;
use Keios\PGPayURO\Classes\HmacHasher;
use October\Rain\Support\Facades\Flash;
use Keios\PGPayURO\Classes\PayURoConnector;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Symfony\Component\HttpFoundation\Response;
use Keios\PaymentGateway\Core\Operator;
use System\Behaviors\SettingsModel;


/**
 * Class PayU
 *
 * @package Keios\PGPayURO
 */
class PayU extends Operator implements StatefulInterface
{
    use SettingsDependent;

    /**
     *
     */
    const CREDIT_CARD_REQUIRED = false;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgpayuro::lang.operators.payuro';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgpayuro/assets/img/payu.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @var PayURoConnector
     */
    private $connector;

    private static $allowedIps = [
        '185.68.12.28', // from failed requests lol
        '185.68.12.26', // from newsletter
        '185.68.12.27', // from newsletter
        '91.194.189.67', // from PDF
        '91.194.189.68', // from PDF
        '91.194.189.69', // from PDF
        '83.96.157.66', // from doc in panel
        '83.96.157.67', // from doc in panel
        '83.96.225.130', // from doc in panel
        '83.96.225.131', // from doc in panel
        '83.96.225.136', // from doc in panel
        '83.96.225.137', // from doc in panel
        '5.134.215.69' // testing ip?
    ];

    /**
     * @var Repository
     */
    private $config;
    /**
     * @var boolean
     */
    private $shouldLog;
    /**
     * @var Log
     */
    private $logger;

    /**
     *
     */
    const AUTHORIZED = 'PAYMENT_AUTHORIZED';
    /**
     *
     */
    const RECEIVED = 'PAYMENT_RECEIVED';
    /**
     *
     */
    const TEST = 'TEST';
    /**
     *
     */
    const CASH = 'CASH';
    /**
     *
     */
    const COMPLETE = 'COMPLETE';
    /**
     *
     */
    const REVERSED = 'REVERSED';
    /**
     *
     */
    const REFUND = 'REFUND';

    /**
     *
     */
    const STATUS_FIELD = 'ORDERSTATUS';

    /**
     * @var array
     */
    private $ipnFields = [
        'SALEDATE',
        'PAYMENTDATE',
        'COMPLETE_DATE',
        'REFNO',
        'REFNOEXT',
        'ORDERNO',
        'ORDERSTATUS',
        'PAYMETHOD',
        'PAYMETHOD_CODE',
        'FIRSTNAME',
        'LASTNAME',
        'IDENTITY_NO',
        'IDENTITY_ISSUER',
        'IDENTITY_CNP',
        'COMPANY',
        'REGISTRATIONNUMBER',
        'FISCALCODE',
        'CBANKNAME',
        'CBANKACCOUNT',
        'ADDRESS1',
        'ADDRESS2',
        'CITY',
        'STATE',
        'ZIPCODE',
        'COUNTRY',
        'PHONE',
        'FAX',
        'CUSTOMEREMAIL',
        'FIRSTNAME_D',
        'LASTNAME_D',
        'COMPANY_D',
        'ADDRESS1_D',
        'ADDRESS2_D',
        'CITY_D',
        'STATE_D',
        'ZIPCODE_D',
        'COUNTRY_D',
        'PHONE_D',
        'IPADDRESS',
        'CURRENCY',
        'IPN_PID',
        'IPN_PNAME',
        'IPN_PCODE',
        'IPN_INFO',
        'IPN_QTY',
        'IPN_PRICE',
        'IPN_VAT',
        'IPN_VER',
        'IPN_DISCOUNT',
        'IPN_PROMONAME',
        'IPN_DELIVEREDCODES',
        'IPN_TOTAL',
        'IPN_TOTALGENERAL',
        'IPN_DATE',
        'HASH',
    ];

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        return $this->bootConnector()->prepareRedirect($this->cart, $this->paymentDetails);
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function processNotification(array $data)
    {
        $this->bootConnector();

        return $this->parseNotification($data);
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        return $this->bootConnector()->sendRefundRequest();
    }

    /**
     * @param array $data
     *
     * @return integer
     * @throws \Exception
     */
    public static function extractUuid(array $data)
    {
	\Log::info(print_r($data, true));
        $httpMethod = Request::method();
        $clientIp = Request::ip();

        if ($httpMethod === 'POST' && !in_array($clientIp, self::$allowedIps)) {
            \Log::error(
                sprintf(
                    'Received POST notification for PayU Ro from IP %s which is not on allowed IPs list (%s).',
                    $clientIp,
                    implode(',', self::$allowedIps)
                )
            );
            (new Response('FORBIDDEN', 403))->send(); // HACK HACK HACK HACK for IP check
            exit;
        }

        if ($httpMethod === 'GET' && !isset($data['pgUuid'])) {
            (new Response('OK', 200))->send(); // HACK HACK HACK HACK for probes from PayU RO
            exit;
        }

        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        } elseif (isset($data['REFNOEXT'])) {
            return $data['REFNOEXT'];
        } else {
            return null;
        }
    }

    /**
     * @return PayURoConnector
     */
    private function bootConnector()
    {
        if (is_null($this->connector)) {
            $this->config = App::make(Repository::class);
            $this->shouldLog = $this->config->get('app.debug', false);
            $this->logger = App::make(Log::class);
            $this->connector = new PayURoConnector($this, $this->logger, $this->shouldLog);
        }

        return $this->connector;
    }

    /**
     * @param array $requestData
     *
     * @return Response
     */
    private function parseNotification(array $requestData)
    {
        if ($this->isCustomerRedirect($requestData)) {
            return $this->handleCustomerRedirect($requestData);
        } else {
            return $this->handleNotification($requestData);
        }
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    private function isCustomerRedirect(array $data)
    {
        return array_key_exists('err', $data) || array_key_exists('ctrl', $data);
    }

    /**
     * @param array $data
     *
     * @return Response
     */
    private function handleCustomerRedirect(array $data)
    {
        if (array_key_exists('err', $data)) {

            /*
             * Possible values of $data['err']:
             * [ACCES DENIED] (sic!) Your access to the PayU interface is not allowed. Please contact the PayU support team.
             * [Invalid account] The MERCHANT parameter is incorrect or not specified.
             * [Access not permitted] You access to the LiveUpdate feature is restricted. You should contact your PayU Account Manager.
             * [Invalid Data] The data you have transmitted is not correctly formed. Please check the arrays.
             * [Invalid product code] The ORDER_PCODE[] array is incorrectly formed
             * [Invalid product name] The ORDER_PNAME[] array is incorrectly formed
             * [Invalid product group] The ORDER_PGROUP[] array is incorrectly formed
             * [Invalid Price] The ORDER_PRICE[] array is incorrectly formed
             * [Invalid VAT] The ORDER_VAT[] array is incorrectly formed
             * [Invalid price] The calculated total is incorrect. Check the DISCOUNT parameters
             * [Invalid Signature] The HMAC_MD5 signature is incorrectly calculated for the sent data
             */

            $this->logError(
                sprintf(
                    'Received error "%s" from PayU RO for payment with UUID %s',
                    $data['err'],
                    $this->payment->uuid
                )
            );

            if ($this->can(static::TRANSITION_REJECT)) {
                $this->rejectFromApi();
            }

            Flash::error(trans('keios.pgpayuro::lang.errors.paymentFailed'));

            return Redirect::to($this->returnUrl);
        }

        if (array_key_exists('ctrl', $data)) {

            /**
             * @var SettingsModel $settings
             */
            $settings = $this->getSettings();
            $hashKey = $settings->get('payu.ro.merchant_key');

            $fullUrl = Request::fullUrl();
            $noQueryString = Request::url();
            $params = Request::all();

            if (!$this->checkUrlHmac($noQueryString, $params, $hashKey)) {
                $this->logInfo(
                    'Received customer redirect with invalid control checksum for url '.$fullUrl.PHP_EOL.
                    'Dumping request data: '.PHP_EOL.print_r($data, true)
                );

                // no info back for you, we don't trust you!

                return Redirect::to('/');
            }

            // tis a legit customer, back to yer order you go, rascal!
            return Redirect::to($this->returnUrl);
        }

        $this->logError(
            'Error handling customer redirect in PayURoConnector - missing error code or control checksum.'
        );

        return Redirect::to($this->returnUrl);
    }

    /**
     * @param array $data
     *
     * @return Response
     */
    private function handleNotification(array $data)
    {
        $pulledFields = [];
        foreach ($this->ipnFields as $allowedField) {
            if (array_key_exists($allowedField, $data)) {
                $pulledFields[$allowedField] = $data[$allowedField];
            }
        }

        $this->logInfo(
            'Received notification from PayURO. Request fields: '.PHP_EOL.print_r(
                $data,
                true
            ).PHP_EOL.'Parsed fields: '.PHP_EOL.print_r($pulledFields, true)
        );

        if (!isset($pulledFields[self::STATUS_FIELD])) {

            $this->logError('Missing status field, returning 400.');

            return new Response('MISSING STATUS', 400);
        }

        if (!$this->payurefno) {
            $this->payurefno = $pulledFields['REFNO'];
        }

        switch ($pulledFields[self::STATUS_FIELD]) {
            case self::AUTHORIZED: // payment was authorized
                // we confirm here because docs don't say which status precisely obliges us to send confirmation.
//                return $this->tryToConfirm($data); // nope
                if ($this->can(static::TRANSITION_ACCEPT)) {
                    $this->accept();
                }

                return $this->generateResponseConfirmation($data);

            case self::CASH:       // payment is going to be paid in cash (sic!)
                return new Response('PAYMENTS API CANNOT ALLOW CUSTOMER TO PAY USING CASH, RETARDS', 417);
            case self::COMPLETE:   // payment is complete - AFTER IDN
                if ($this->can(static::TRANSITION_ACCEPT)) {
                    $this->accept();
                }

                return $this->generateResponseConfirmation($data);
            case self::RECEIVED:   // payment received by gateway
                // we confirm here because docs don't say which status precisely obliges us to send confirmation.
                return $this->tryToConfirm($data);
            case self::REFUND:     // payment refunded
                $this->refund();

                return $this->generateResponseConfirmation($data);
            case self::REVERSED:   // order reversed
                $this->refund();

                return $this->generateResponseConfirmation($data);
            case self::TEST:
                $this->testPayment = true;

                if ($this->can(static::TRANSITION_ACCEPT)) {
                    $this->accept();
                }

                return $this->generateResponseConfirmation($data);
            default:
                $this->logError(sprintf('Unknown status %s received.', $pulledFields[self::STATUS_FIELD]));

                return new Response('UNKNOWN STATUS', 400);
        }
    }

    /**
     * @param string $baseUrl
     * @param array  $queryParams
     * @param string $key
     *
     * @return bool
     */
    private function checkUrlHmac($baseUrl, array $queryParams, $key)
    {
        $ctrl = $queryParams['ctrl'];

        unset($queryParams['ctrl']);

        $url = $baseUrl.'?'.http_build_query($queryParams);

        $toCalculate = strlen($url).$url;
        $hmac = hash_hmac("md5", $toCalculate, $key);

        return $hmac === $ctrl;
    }


    /**
     * @param string $msg
     */
    private function logInfo($msg)
    {
        if ($this->shouldLog) {
            $this->logger->log('info', $msg);
        }
    }

    /**
     * @param string $msg
     */
    private function logError($msg)
    {
        if ($this->shouldLog) {
            $this->logger->log('error', $msg);
        }
    }

    /**
     * @param array $data
     *
     * @return Response
     */
    private function generateResponseConfirmation(array $data)
    {
        /**
         * @var SettingsModel $settings
         */
        $settings = $this->getSettings();
        $hashKey = $settings->get('payu.ro.merchant_key');
        $date = Carbon::now()->format('YmdHis');
        $hash = $this->makeResponseConfirmationHash($data, $date, $hashKey);

        return Response("<EPAYMENT>$date|$hash</EPAYMENT>", 200);
    }

    /**
     * @param array $data
     * @param       $date
     * @param       $hashKey
     *
     * @return string
     */
    private function makeResponseConfirmationHash(array $data, $date, $hashKey)
    {
        $list = [$data['IPN_PID'][0], $data['IPN_PNAME'][0], $data['IPN_DATE'], $date];

        return HmacHasher::calculate($list, $hashKey);
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    private function confirm(array $data)
    {
        return $this->connector->sendConfirmation($data);
    }

    /**
     * @param array $data
     *
     * @return Response
     */
    private function tryToConfirm(array $data)
    {
        if ($this->confirm($data)) {
            $this->saveWithState();

            return $this->generateResponseConfirmation($data);
        } else {
            return new Response('COULD NOT CONFIRM ORDER', 500);
        }
    }
}