<?php namespace Keios\PGPayURO;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\URL;
use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use Keios\PaymentGateway\ValueObjects\Item;
use System\Classes\PluginBase;

/**
 * PG-PayU-RO Plugin Information File
 *
 * @package Keios\PGPayURO
 */
class Plugin extends PluginBase
{
    /**
     * @var array
     */
    public $require = [
        'Keios.PaymentGateway'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-PayU-RO',
            'description' => 'keios.pgpayuro::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-dollar'
        ];
    }

    /**
     *
     */
    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGPayURO\Operators\PayU');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'payu.general'         => [
                            'label' => 'keios.pgpayuro::lang.settings.general',
                            'tab'   => 'keios.pgpayuro::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'payu.info'            => [
                            'type' => 'partial',
                            'path' => '$/keios/pgpayuro/partials/_payu_info.htm',
                            'tab'  => 'keios.pgpayuro::lang.settings.tab',
                        ],
                        'payu.ro.ipn_url'      => [
                            'label' => 'keios.pgpayuro::lang.settings.ipn_url',
                            'tab'   => 'keios.pgpayuro::lang.settings.tab',
                            'type'  => 'partial',
                            'path'  => '$/keios/pgpayuro/partials/url.htm'
                        ],
                        'payu.ro.merchant_id'  => [
                            'label'   => 'keios.pgpayuro::lang.settings.merchant_id',
                            'tab'     => 'keios.pgpayuro::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'payu.ro.merchant_key' => [
                            'label'   => 'keios.pgpayuro::lang.settings.merchant_key',
                            'tab'     => 'keios.pgpayuro::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'payu.ro.mode'         => [
                            'label' => 'keios.pgpayuro::lang.settings.testMode',
                            'tab'   => 'keios.pgpayuro::lang.settings.tab',
                            'type'  => 'switch'
                        ],
                    ]
                );
            }
        );
    }
}

